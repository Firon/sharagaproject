#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include "Table.h"
#include "Libs.h"
#include <iomanip>
using namespace std;
class Menu {
private:
	vector<Table> list;
	Menu(const Menu&) {}
	const Menu& operator=(const Menu&) {}
public:
	Menu();
	static Menu& getObject();
	void inputData();
	void outputData();
	void editData();
	void deleteData();
	void loadFromFile();
	void saveToFile();
};
