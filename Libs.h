#pragma once
#include <iostream>
#include <stdio.h>
#include <iomanip>
#ifndef __linux
#include <conio.h>
#include <cstdlib>
void  inline clear() {
	system("cls");
}
#else
#include <unistd.h>
#include <termios.h>
#include <cstdlib>
// int _getch()
// {
// 	int ch;
// 	struct termios oldt, newt;
// 	tcgetattr(STDIN_FILENO, &oldt);
// 	newt = oldt;
// 	newt.c_lflag &= ~(ICANON | ECHO);
// 	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
// 	ch = getchar();
// 	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
// 	return ch;
// }
void inline clear()
{
	system("clear");
}
#endif