#pragma once
#include <string>
#include <vector>
#include <string>

class Table 
{
public:

	enum Type_operation
	{
		income,
		expense
	};
	Table() : date(""), description(""), type((Type_operation)0), amount(0) {};
	static Table& getObject();
	void Add(std::string &date, Type_operation type, size_t amount, std::string &description);
	bool Delete(const size_t& number, std::vector<Table>& vec);
	std::string getDate();
	std::string getDescription();
	size_t getAmount();
	Type_operation getType();
	std::string getTypeToStr();
	~Table() {}
private:
	std::string date, description;
	Type_operation type;
	size_t amount;
};